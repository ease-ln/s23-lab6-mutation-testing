from bonus_system import calculateBonuses


def test_standard():
    assert calculateBonuses("Standard", 0) == 0.5
    assert calculateBonuses("Standard", 10000) == 1.5 * 0.5
    assert calculateBonuses("Standard", 50000) == 2 * 0.5
    assert calculateBonuses("Standard", 100000) == 2.5 * 0.5
    assert calculateBonuses("Standard", 200000) == 2.5 * 0.5


def test_premium():
    assert calculateBonuses("Premium", 0) == 0.1
    assert calculateBonuses("Premium", 10000) == 1.5 * 0.1
    assert calculateBonuses("Premium", 50000) == 2 * 0.1
    assert calculateBonuses("Premium", 100000) == 2.5 * 0.1
    assert calculateBonuses("Premium", 200000) == 2.5 * 0.1


def test_diamond():
    assert calculateBonuses("Diamond", 0) == 0.2
    assert calculateBonuses("Diamond", 10000) == 1.5 * 0.2
    assert calculateBonuses("Diamond", 50000) == 2 * 0.2
    assert calculateBonuses("Diamond", 100000) == 2.5 * 0.2
    assert calculateBonuses("Diamond", 200000) == 2.5 * 0.2


def test_invalid():
    assert calculateBonuses("A", 0) == 0
    assert calculateBonuses("Z", 10000) == 0